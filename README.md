# Fetch Rewards Exercise Solution for Tony Perriello
This was written using Angular 10.

# Running the app

After cloning, navigate to the cloned directory and run the following commands:

1.  `npm install`

2.  `ng serve`

The app will be accessible via http://localhost:4200.

# Thank you!
This was fun! Thank you for your time and your consideration.
