import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { FrAppComponent } from './app.component';
import { FrListComponent } from './list/list.component';

@NgModule({
  declarations: [
    FrAppComponent,
    FrListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [FrAppComponent]
})
export class AppModule { }
