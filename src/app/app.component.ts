import { Component } from '@angular/core';

@Component({
  selector: 'fr-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class FrAppComponent {}
