import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ListService } from './list.service';
import { ListItem } from './list.model';

@Component({
  selector: 'fr-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class FrListComponent implements OnInit, OnDestroy {
  listItems: Map<number, ListItem[]>;
  sortedListIds: number[];
  private listServiceSubscription: Subscription;

  constructor(private readonly listService: ListService) { }

  ngOnInit(): void {
    this.listServiceSubscription = this.listService.get()
      .subscribe((listItems: Map<number, ListItem[]>) => {
        this.listItems = listItems;
        this.sortedListIds = Array.from(this.listItems.keys()).sort();
      });
  }

  ngOnDestroy(): void {
    this.listServiceSubscription.unsubscribe();
  }

  getListItemsSortedByListId(listId: number): ListItem[] {
    return this.listItems.get(listId).sort((a, b) => {
      return this.getListItemNameNumber(a.name) < this.getListItemNameNumber(b.name) ? -1 : 0;
    });
  }

  private getListItemNameNumber(name: string): number {
    return parseInt(name.split(' ')[1]);
  }
}
