export interface ListItem {
  id: number;
  listId: number;
  name: string;
}
