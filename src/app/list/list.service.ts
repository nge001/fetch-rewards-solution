import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ListItem } from './list.model';

@Injectable({
  providedIn: 'root'
})
export class ListService {
  constructor(private readonly http: HttpClient) {}

  get(): Observable<Map<number, ListItem[]>> {
    return this.http.get('fetch-hiring/hiring.json')
      .pipe(
        map(this.refineListData),
        catchError((err) => Observable.throw(err))
      );
  }

  private refineListData(data: ListItem[]): Map<number, ListItem[]> {
    const listDataByListId: Map<number, ListItem[]> = new Map<number, ListItem[]>();

    for (let i = 0; i < data.length; i++) {
      const item = (data[i] as ListItem);

      // If name is falsy, ignore it.
      if (!item.name) {
        continue;
      }

      // If Map has listId, append item to array.
      // Else, create key with this item as the first element.
      if (listDataByListId.has(item.listId)) {
        listDataByListId.get(item.listId).push(item);
      } else {
        listDataByListId.set(item.listId, [item]);
      }
    }

    return listDataByListId;
  }
}
